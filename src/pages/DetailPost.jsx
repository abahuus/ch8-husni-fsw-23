import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const DetailPost = () => {
  const { postId } = useParams();
  const [post, setPost] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    axios      
      .get(`https://binar-blog-app.herokuapp.com/posts/${postId}`)
      .then((res) => setPost(res.data))
      .catch((err) => console.error(err));
  });

  return (
    <> 
      <main>
          <div className="container-fluid bg-trasparent my-4 p-3" style={{position: "relative"}}>
              <div className="row row-cols-2 row-cols-xs-4 row-cols-sm-4 row-cols-lg-4 g-5 text-center">              
                  <div className="col">
                      <div className="card h-100 shadow-sm">
                          <img src={post.image} className="img-fluid" alt="..." />  
                          <div className="label-top shadow-sm">{post.title}</div>                  
                          <div className="card-body">                                
                              <h5 className="card-title me-3" style={{ textAlign: "justify"}}>
                                {post.body}
                              </h5>
                              <div className="text-center">                              
                                <button type="button" className="btn btn-primary btn-sm me-2" onClick={() => {
                                    navigate(-1);
                                  }}>Back
                                </button>
                              </div>                               
                          </div>
                      </div>
                  </div>                       
              </div>
          </div>
      </main>
  </>
  );
};

export default DetailPost;
