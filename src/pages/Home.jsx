import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import React, { useEffect, useState } from "react";
import "../App.css";
import { useNavigate } from "react-router-dom";

const Home = () => {
    const [posts, setPosts] = useState([]);
    const navigate = useNavigate(); 
  
    const fetchPosts = () => {
      axios
        .get("https://binar-blog-app.herokuapp.com/posts")      
        .then((res) => {
          const listPosts = res.data;        
          setPosts(listPosts);
        })
        .catch((err) => console.error(err));
    };
    
    const handleClick = (id) => {
      navigate(`/${id}`);
    };

    useEffect(() => {
      fetchPosts();
    }, []);
  
    return (     
    <>        
      <main>
          <div className="container-fluid bg-trasparent my-4 p-3" style={{position: "relative"}}>
              <div className="row row-cols-1 row-cols-xs-2 row-cols-sm-2 row-cols-lg-4 g-5">                  
                  {posts.map((post) => (  
                    <div className="col" key={post.id}>
                        <div className="card h-100 shadow-sm">
                            <img src={post.image} className="img-fluid" alt="..." />                            
                            <div className="card-body">                                
                                <h5 className="card-title me-3" style={{ textAlign: "justify"}}>
                                  {post.title}
                                </h5>
                                <div className="text-center">                                 
                                  <button type="button" onClick={() => handleClick(post.id)} className="btn btn-primary btn-sm me-2">Detail</button> 
                                </div>                               
                            </div>
                        </div>
                    </div>
                  ))}                 
              </div>
          </div>
      </main> 
    </>
    );
};

export default Home;