import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const Register = () => {  

    const bgImage = new URL("https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp")
    
    const [values, setValues] = useState({ name: "", email: "", password: "" });
    const navigate = useNavigate(); 

    const handleOnChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {       
        e.preventDefault();
        axios
          .post("https://binar-blog-app.herokuapp.com/registration", values)
          .then((res) => {           
            navigate("/login");
          })
          .catch((err) => alert("Maaf, Registrasi anda masih salah"));
    };  

    return (             
        <section className="vh-100" style={{backgroundColor: "#eee"}}>
            <div className="container h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                <div className="col-lg-12 col-xl-11">                  
                    <div className="card text-black" style={{ borderRadius: "25px" }}>
                        <div className="card-body p-md-5">
                            <div className="row justify-content-center">
                            <div className="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                                <p className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign Up</p>
                                <form className="mx-1 mx-md-4" onSubmit={handleSubmit}>
                                <div className="d-flex flex-row align-items-center mb-4">                                    
                                    <div className="form-outline flex-fill mb-0">                                       
                                        <input type="tex" id="name" name="name" onChange={handleOnChange} value={values.name}
                                            className="form-control" placeholder="Your Fullname"/>                                   
                                    </div>
                                </div>
                                <div className="d-flex flex-row align-items-center mb-4">                                   
                                    <div className="form-outline flex-fill mb-0">
                                    <input type="email" id="email" name="email" onChange={handleOnChange} value={values.email}
                                        className="form-control" placeholder="Your Email as Username"/>                                 
                                    </div>
                                </div>
                                <div className="d-flex flex-row align-items-center mb-4">                                  
                                    <div className="form-outline flex-fill mb-0">
                                        <input type="password" id="password" name="password" onChange={handleOnChange} value={values.password}
                                        className="form-control" placeholder="Password" />
                                                                      
                                    </div>
                                </div>                                
                                <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                    <button className="btn btn-primary btn-sm" type="submit">Register</button>
                                </div>
                                </form>
                            </div>
                            <div className="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">                                
                                <img src={bgImage} className="img-fluid" alt=""/>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    );
}

export default Register;

// function Register() {
//     const bgImage = new URL("https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp")
//     // https://mdbootstrap.com/docs/standard/extended/registration/
//     return (
//         // <div>
//         //     <div className="container mt-4">
//         //         <div className="row">                
//         //             <div className="ml-2">
//         //                 <form >
//         //                     <div className="mb-4">
//         //                         <label htmlFor="fullname" className="form-label">Full Name</label>
//         //                         <input  type="text" className="form-control" id="fullname"  placeholder="Full Name"  />
//         //                     </div>
//         //                     <div className="mb-4">
//         //                         <label htmlFor="text" className="form-label">Username</label>
//         //                         <input type="text"  className="form-control" id="username" placeholder="username or email" />
//         //                     </div>
//         //                     <div className="mb-4">
//         //                         <label htmlFor="password" className="form-label">Password</label>
//         //                         <input type="password"  className="form-control" id="password" placeholder="password" />
//         //                     </div>                       
//         //                     <button type="submit" className="btn btn-primary me-2">Submit</button>
//         //                     {/* <button type="button" className="btn btn-danger">Reset</button> */}
//         //                 </form>
//         //             </div>    
//         //         </div>
//         //     </div>
//         // </div>       
//         <section className="vh-100" style={{backgroundColor: "#eee"}}>
//             <div className="container h-100">
//                 <div className="row d-flex justify-content-center align-items-center h-100">
//                 <div className="col-lg-12 col-xl-11">                  
//                     <div className="card text-black" style={{ borderRadius: "25px" }}>
//                         <div className="card-body p-md-5">
//                             <div className="row justify-content-center">
//                             <div className="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
//                                 <p className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign Up</p>
//                                 <form className="mx-1 mx-md-4" method="post">
//                                 <div className="d-flex flex-row align-items-center mb-4">
//                                     {/* <i className="fas fa-user fa-lg me-3 fa-fw"></i> */}
//                                     <div className="form-outline flex-fill mb-0">
//                                         <input type="text" id="form3Example1c" className="form-control" placeholder="Your Fullname" />
//                                         {/* <input id ="fullname" type="text" name="fullname" placeholder="Enter Fullname" Fullname="form-control" /> */}

//                                     {/* <label className="form-label" for="form3Example1c">Your Fullname</label> */}
//                                     </div>
//                                 </div>
//                                 <div className="d-flex flex-row align-items-center mb-4">
//                                     {/* <i className="fas fa-envelope fa-lg me-3 fa-fw"></i> */}
//                                     <div className="form-outline flex-fill mb-0">
//                                      <input type="email" id="form3Example3c" className="form-control" placeholder="Your Email as Username"/>
//                                     {/* <label className="form-label" for="form3Example3c">Your Email or Username</label> */}
//                                     </div>
//                                 </div>
//                                 <div className="d-flex flex-row align-items-center mb-4">
//                                     {/* <i className="fas fa-lock fa-lg me-3 fa-fw"></i> */}
//                                     <div className="form-outline flex-fill mb-0">
//                                         <input type="password" id="form3Example4c" className="form-control" placeholder="Password" />
//                                     {/* <label className="form-label" for="form3Example4c">Password</label> */}
//                                     </div>
//                                 </div>
//                                 {/* <div className="d-flex flex-row align-items-center mb-4">
//                                     <i className="fas fa-key fa-lg me-3 fa-fw"></i>
//                                     <div className="form-outline flex-fill mb-0">
//                                     <input type="password" id="form3Example4cd" className="form-control" />
//                                     <label className="form-label" for="form3Example4cd">Repeat your password</label>
//                                     </div>
//                                 </div>
//                                 <div className="form-check d-flex justify-content-center mb-5">
//                                     <input className="form-check-input me-2" type="checkbox" value="" id="form2Example3c" />
//                                     <label className="form-check-label" for="form2Example3">
//                                     I agree all statements in <a href="#!">Terms of service</a>
//                                     </label>
//                                 </div> */}
//                                 <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
//                                     <button type="button" className="btn btn-primary btn-lg">Register</button>
//                                 </div>
//                                 </form>
//                             </div>
//                             <div className="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">
//                                 {/* <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"
//                                 className="img-fluid" alt="Sample image"> */}
//                                 <img src={bgImage} className="img-fluid" alt=""/>
//                             </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//                 </div>
//             </div>
//         </section>
//     );
// }

// export default Register;