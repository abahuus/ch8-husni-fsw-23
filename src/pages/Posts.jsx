import axios from "axios";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";

const  Posts = () => {

    const [values, setValues] = useState({ title: "", image: "", body: "" });
    const [cookies] = useCookies(["accessToken", "userId"]);
    const navigate = useNavigate();

    const handleOnChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {
        e.preventDefault()
        axios
          .post("https://binar-blog-app.herokuapp.com/posts", values,
          {
            headers: { Authorization: `Bearer ${cookies.accessToken}` }
          })
          .then((res) => {           
            navigate("/dashboard");
          })
          .catch((err) => alert("Save Error .."));
    };

    useEffect(() => {         
        if (!cookies.accessToken && !cookies.userId) {
          alert("Anda Harus Login Untuk Membuat Artikel ..");
          navigate("/login");
        }      
      }, [cookies, navigate]);

    return (           
        <section className="vh-100" style={{backgroundColor: "#eee"}}>
            <div className="container h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-lg-12 col-xl-11">                  
                        <div className="card text-black" style={{ borderRadius: "25px" }}>
                            <div className="card-body p-md-5">
                                <div className="row justify-content-center">
                                    <div className="col-md-10 col-lg-6 col-xl-11 order-2 order-lg-1">
                                        <p className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Create Post</p>
                                        <form className="mx-1 mx-md-4" onSubmit={handleSubmit}>                                        
                                            <div className="d-flex flex-row align-items-center mb-4">                                               
                                                <div className="form-outline flex-fill mb-0">                                                
                                                <input type="tex" id="title" name="title" onChange={handleOnChange} value={values.title}
                                                    className="form-control" placeholder="Title"/>                                    
                                                </div>
                                            </div>
                                            <div className="d-flex flex-row align-items-center mb-4">                                               
                                                <div className="form-outline flex-fill mb-0">
                                                <input type="tex" id="image" name="image" onChange={handleOnChange} value={values.image}
                                                    className="form-control" placeholder="Link URL Image"/>                                   
                                                </div>
                                            </div>
                                            <div className="d-flex flex-row align-items-center mb-4">                                               
                                                <div className="form-outline flex-fill mb-0">
                                                <textarea rows="4"id="body" name="body" onChange={handleOnChange} value={values.body}
                                                    className="form-control" placeholder="Content of Article">
                                                </textarea>                                                                                
                                                </div>
                                            </div>                                  
                                            <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                                <button type="submit" className="btn btn-primary btn-sm">Save</button>
                                            </div>
                                        </form>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Posts;