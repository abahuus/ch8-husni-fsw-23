import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import "../App.css";
import { useNavigate } from "react-router-dom";

const Dashboard = () => {
    const [posts, setPosts] = useState([]);
    const [cookies] = useCookies(["accessToken", "userId"]);
    const navigate = useNavigate();
  
    const fetchPosts = () => {    
      axios
        .get(`https://binar-blog-app.herokuapp.com/posts?writer=${cookies.userId}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` }
        }) 
        .then((res) => {
          const listPosts = res.data;        
          setPosts(listPosts);
        })
        .catch((err) => console.error(err));
    };    
  
    const handleClick = (id) => {
      navigate(`/blogs/${id}`);
    };

    useEffect(() => {  
      console.log('tes1 ' + cookies.userId); 
      if (!cookies.accessToken && !cookies.userId) {
        alert("Anda Harus Login Untuk Melihat Dashboard User..");
        navigate("/login");
      }
      fetchPosts();     
    }, [cookies, navigate]);
  
    return (         
    <> 
      <main>
          <div className="container-fluid bg-trasparent my-4 p-3" style={{position: "relative"}}>
              <div className="me-3">
                <a href="/posts" className="btn btn-sm btn-info">Create New Post</a>
              </div> 
              <div className="row row-cols-1 row-cols-xs-2 row-cols-sm-2 row-cols-lg-4 g-5">                 
                  {posts.map((post) => (  
                    <div className="col" key={post.id}>
                        <div className="card h-100 shadow-sm">
                            <img src={post.image} className="img-fluid" alt="..." />
                            {/* <div className="label-top shadow-sm">{post.title}</div> */}
                            <div className="card-body">                               
                                <h5 className="card-title me-3" style={{ textAlign: "justify"}}>
                                  {post.title}
                                </h5>
                                <div className="text-center">
                                  <a href="/edit" className="btn btn-sm btn-warning">Edit</a>
                                </div>                               
                            </div>
                        </div>
                    </div>
                  ))}                
              </div>
          </div>
      </main> 
    </>
    );
};

export default Dashboard;