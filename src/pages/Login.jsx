import axios from "axios";
import React, { useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";

const Login = () => {
    const [values, setValues] = useState({ email: "" });
    const [cookies, setCookies] = useCookies(["accessToken", "userId", "email"]);
    const navigate = useNavigate();

    const handleOnChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };

    const bgImage = new URL("https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp")    
    // https://mdbootstrap.com/docs/standard/extended/login/

    const handleSubmit = (e) => {
        e.preventDefault()
        axios
          .post("https://binar-blog-app.herokuapp.com/login", values)
          .then((res) => {
            const { accessToken, id, email } = res.data;
            setCookies("accessToken", accessToken, { maxAge: 60000 });
            setCookies("userId", id, { maxAge: 60000 });
            setCookies("email", email, { maxAge: 60000 });
            navigate("/dashboard");
          })
          .catch((err) => alert("Login Anda masih salah.."));
    };

    return (            
        <section className="vh-100" style={{backgroundColor: "#eee"}}>
            <div className="container h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                <div className="col-lg-12 col-xl-11">                  
                    <div className="card text-black" style={{ borderRadius: "25px" }}>
                        <div className="card-body p-md-5">
                            <div className="row justify-content-center">
                            <div className="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                                <p className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign In</p>
                                <form className="mx-1 mx-md-4" onSubmit={handleSubmit}>                               
                                    <div className="d-flex flex-row align-items-center mb-4">                                        
                                        <div className="form-outline flex-fill mb-0">
                                        <input type="email" id="email" name="email" onChange={handleOnChange} value={values.email}
                                            className="form-control" placeholder="Your Email as Username"/>                                   
                                        </div>
                                    </div>
                                    <div className="d-flex flex-row align-items-center mb-4">                                        
                                        <div className="form-outline flex-fill mb-0">
                                        <input type="password" id="password" name="password" onChange={handleOnChange}
                                            className="form-control" placeholder="Password" />                                   
                                        </div>
                                    </div>                                
                                    <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                        <button className="btn btn-primary btn-sm" type="submit" >Login
                                        </button>                                       
                                    </div>
                                    <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">                                        
                                        <div class="text-center">
                                            <p>Not a member? <a href="/register">Register</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">                               
                                <img src={bgImage} className="img-fluid" alt=""/>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    );
}

export default Login;


// function Login() {
//     const bgImage = new URL("https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp")    
//     // https://mdbootstrap.com/docs/standard/extended/login/
//     return (           
//         <section className="vh-100" style={{backgroundColor: "#eee"}}>
//             <div className="container h-100">
//                 <div className="row d-flex justify-content-center align-items-center h-100">
//                 <div className="col-lg-12 col-xl-11">                  
//                     <div className="card text-black" style={{ borderRadius: "25px" }}>
//                         <div className="card-body p-md-5">
//                             <div className="row justify-content-center">
//                             <div className="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
//                                 <p className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign In</p>
//                                 <form className="mx-1 mx-md-4">
//                                 {/* <div className="d-flex flex-row align-items-center mb-4">
//                                     <i className="fas fa-user fa-lg me-3 fa-fw"></i>
//                                     <div className="form-outline flex-fill mb-0">
//                                     <input type="text" id="form3Example1c" className="form-control" placeholder="Your Fullname" />                                   
//                                     </div>
//                                 </div> */}
//                                 <div className="d-flex flex-row align-items-center mb-4">
//                                     <i className="fas fa-envelope fa-lg me-3 fa-fw"></i>
//                                     <div className="form-outline flex-fill mb-0">
//                                     <input type="email" id="form3Example3c" className="form-control" placeholder="Your Email or Username"/>                                   
//                                     </div>
//                                 </div>
//                                 <div className="d-flex flex-row align-items-center mb-4">
//                                     <i className="fas fa-lock fa-lg me-3 fa-fw"></i>
//                                     <div className="form-outline flex-fill mb-0">
//                                     <input type="password" id="form3Example4c" className="form-control" placeholder="Password" />                                   
//                                     </div>
//                                 </div>                                
//                                 <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
//                                     <button type="button" className="btn btn-primary btn-lg">Login</button>
//                                 </div>
//                                 </form>
//                             </div>
//                             <div className="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">                               
//                                 <img src={bgImage} className="img-fluid" alt=""/>
//                             </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//                 </div>
//             </div>
//         </section>
//     );
// }

// export default Login;