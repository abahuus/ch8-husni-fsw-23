import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Outlet } from "react-router-dom";
import "../App.css"

function AppNavbar() {
  const bgImage = new URL("../../public/logo192.png", import.meta.url)
  return (
    <>
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container>
        <Navbar.Brand className="me-5" href="/">
        <div className='picture d-flex align-items-center'>
          <img src={bgImage} alt=""/>
        </div>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav>
            <Nav.Link className="text-white ms-5 me-5" href="/">Home</Nav.Link>
            <Nav.Link className="text-white ms-5 me-5"href="/dashboard">Dashboard</Nav.Link> 
            <Nav.Link className="text-white ms-5 me-5"href="/register">Register</Nav.Link> 
            <Nav.Link className="text-white ms-5 me-5"href="/login">Login</Nav.Link>
            <Nav.Link className="text-white ms-5 me-5"href="/posts">Posts</Nav.Link>           
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar> 
    {/* <div
        style={{
          backgroundColor: "lightgreen",
          padding: "10px",
        }}
      >
        <div style={{ backgroundColor: "white", minHeight: "500px" }}>
          <Outlet />          
        </div>
    </div> */}
    </>
  );
}

export default AppNavbar;
