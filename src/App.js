import { Route, Routes } from "react-router-dom";
import AppNavbar from "./pages/AppNavbar";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Posts from "./pages/Posts";
import Dashboard from "./pages/Dashboard";
import Home from "./pages/Home";
import EditPost from "./pages/EditPost";
import DetailPost from "./pages/DetailPost";

const App = () => {
  return (
    <>
    <AppNavbar />
      <Routes>       
        <Route path="/" element={<Home />} />
        <Route path="/:postId" element={<DetailPost />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />     
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/posts" element={<Posts />} />   
        <Route path="/edit" element={<EditPost />} />     
      </Routes>     
    </>
  );
};

export default App;